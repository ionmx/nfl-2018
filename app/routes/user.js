import Route from '@ember/routing/route';

export default Route.extend({
  setupController: function(controller, model) {
    controller.set('model', model.reload());
    controller.set('viewUserID', model.get('id'));

     this.store.findAll('match').then(function(matches) {
       controller.set('matches', matches);
     });

  },
  beforeModel: function() {

  }
});

