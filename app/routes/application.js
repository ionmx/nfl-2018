import Route from '@ember/routing/route';
import { inject } from '@ember/service';

export default Route.extend({
  session: inject(),
  beforeModel: function() {
    return this.get('session')
               .fetch()
               .catch(function() {});
  },

  renderTemplate: function() {
    this.render();
    this.render('users', {
      into: 'application',
      outlet: 'ranking',
      controller: 'users',
      model: this.store.findAll('user')
    });
  },
  actions: {
    signIn: function(provider) {
      var self = this
      this.get('session').open('firebase', { provider: provider}).then(function(data) {

        self.store.find('user', data.currentUser.uid).then(function(user) {
          // Logged
          self.transitionTo('user', user.id);
        }, function(reason) {
          // Create user...
          var properties = {
            id: data.currentUser.uid,
            name: data.currentUser.displayName,
            displayName: data.currentUser.displayName,
            photoUrl: data.currentUser.photoURL,
            score: 0
          };
          var u = self.store.createRecord('user', properties);
          u.save().then(function() {



            // Add empty predictions...
            self.store.findAll('match').then(function(matches) {
              matches.forEach(function(m) {
                var predProperties = {
                  'id': [u.id, '_', m.id].join(''),
                  'date': new Date().getTime(),
                  'homePrediction': null,
                  'visitorPrediction': null,
                };
                var p = self.store.createRecord('prediction', predProperties);
                p.save().then(function() {
                  u.get('predictions').then(function(predictions) {
                    predictions.addObject(p);
                    u.save();
                  });
                });
              });
            });

            // Go to user
            self.transitionTo('user', u.id);

          });
        });
      });
    },
    signOut: function() {
      this.controllerFor("application").set('viewUserID', null);
      this.controllerFor("application").set('currentUser', null);
      this.get('session').close();
    }
  }
});
