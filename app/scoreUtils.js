export function getOrdinal(n) {
   var s=["th","st","nd","rd"],
       v=n%100;
   return n+'<sup>'+(s[(v-20)%10]||s[v]||s[0])+'</sup>';
}

export function getWinner(home, away) {
  var winner = '';
  if (home > away) {
    winner = 'home';
  } else if (home < away) {
    winner = 'away';
  } else {
    winner = 'tied';
  }
  return winner;
}

export function getScore(home, away, homePrediction, awayPrediction) {
  var points = 0;
  if (home >= 0 && homePrediction != null && awayPrediction != null) {
    if ( (home == homePrediction) && (away == awayPrediction) ) {
      points = 15;
    } else if (getWinner(home, away) == getWinner(homePrediction, awayPrediction)) {
      points = 10 - Math.abs(homePrediction - home) - Math.abs(awayPrediction - away);
      if (points < 0) { points = 0; }
    }
  }
  if (isNaN(points)) {
    points = 0;
  }
  return points;
}

export default { 
  getOrdinal: getOrdinal,
  getWinner: getWinner,
  getScore: getScore 
};

