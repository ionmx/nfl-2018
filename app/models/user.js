import DS from 'ember-data';

export default DS.Model.extend({
  name:         DS.attr('string'),
  displayName:  DS.attr('string'),
  photoUrl:    DS.attr('string'),
  score:        DS.attr('number'),
  predictions:  DS.hasMany('prediction', { inverse: 'user', async: true }),
});

