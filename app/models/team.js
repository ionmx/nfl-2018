import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  name:  DS.attr('string'),
  conference: DS.attr('string'),
  division: DS.attr('string'),

  logo: computed('id', function() {
    return `/assets/images/${this.get('id')}.svg`;
  })
});

