import DS from 'ember-data';

export default DS.Model.extend({
  date:              DS.attr('number'),
  user:              DS.belongsTo('user', { async: true }),
  homePrediction:    DS.attr('number'),
  awayPrediction: DS.attr('number')
});

